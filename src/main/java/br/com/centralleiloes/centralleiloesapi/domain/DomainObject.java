package br.com.centralleiloes.centralleiloesapi.domain;

public interface DomainObject {

    Integer getId();
    void setId(Integer id);
}
