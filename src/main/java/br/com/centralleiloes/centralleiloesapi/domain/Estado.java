package br.com.centralleiloes.centralleiloesapi.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "estados")
public class Estado extends AbstractDomainClass {

    private String nome;
    private String sigla;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
}
