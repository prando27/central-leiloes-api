package br.com.centralleiloes.centralleiloesapi.service;

import java.util.List;

public interface CrudService<T, ID> {

    List<T> findAll();
    T findOne(ID id);
    T save(T t);
    void delete(ID id);
}
