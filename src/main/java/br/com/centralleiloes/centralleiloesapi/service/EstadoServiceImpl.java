package br.com.centralleiloes.centralleiloesapi.service;

import br.com.centralleiloes.centralleiloesapi.domain.Estado;
import br.com.centralleiloes.centralleiloesapi.repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EstadoServiceImpl implements EstadoService {

    private EstadoRepository estadoRepository;

    @Autowired
    public void setEstadoRepository(EstadoRepository estadoRepository) {
        this.estadoRepository = estadoRepository;
    }

    @Override
    public List<Estado> findAll() {
        List<Estado> estados = new ArrayList<>();
        this.estadoRepository.findAll().forEach(estados::add);
        return estados;
    }

    @Override
    public Estado findOne(Integer id) {
        return this.estadoRepository.findOne(id);
    }

    @Override
    public Estado save(Estado estado) {
        return this.estadoRepository.save(estado);
    }

    @Override
    public void delete(Integer id) {
        this.estadoRepository.delete(id);
    }
}
