package br.com.centralleiloes.centralleiloesapi.service;

import br.com.centralleiloes.centralleiloesapi.domain.Estado;

public interface EstadoService extends CrudService<Estado, Integer> {
}
