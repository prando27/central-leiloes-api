package br.com.centralleiloes.centralleiloesapi.controller;

import br.com.centralleiloes.centralleiloesapi.controller.dto.EstadoDto;
import br.com.centralleiloes.centralleiloesapi.domain.Estado;
import br.com.centralleiloes.centralleiloesapi.service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/estados")
public class EstadoController {

    private EstadoService estadoService;

    @Autowired
    public void setEstadoService(EstadoService estadoService) {
        this.estadoService = estadoService;
    }

    @GetMapping
    public List<EstadoDto> findAll() {
        return this.estadoService.findAll().stream().map(EstadoDto::fromEstado).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public EstadoDto findOne(@PathVariable Integer id) {
        return EstadoDto.fromEstado(this.estadoService.findOne(id));
    }

    @PostMapping
    public EstadoDto save(@RequestBody EstadoDto estadoDto) {
        Estado estado = new Estado();
        estado.setNome(estadoDto.getNome());
        estado.setSigla(estadoDto.getSigla());

        return EstadoDto.fromEstado(this.estadoService.save(estado));
    }

    @PutMapping("/{id}")
    public EstadoDto update(@PathVariable Integer id, @RequestBody EstadoDto estadoDto) {
        Estado estado = this.estadoService.findOne(id);
        if (estado == null) {
            throw new RuntimeException("Estado não encontrado.");
        }

        estado.setNome(estadoDto.getNome());
        estado.setSigla(estadoDto.getSigla());

        return EstadoDto.fromEstado(this.estadoService.save(estado));
    }

    @DeleteMapping("/{id}")
    public void delete(Integer id) {
        this.estadoService.delete(id);
    }
}
