package br.com.centralleiloes.centralleiloesapi.controller.dto;

import br.com.centralleiloes.centralleiloesapi.domain.Estado;

public class EstadoDto {

    private String id;
    private String nome;
    private String sigla;

    public EstadoDto() {
    }

    private EstadoDto(String id, String nome, String sigla) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
    }

    public String getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getSigla() {
        return sigla;
    }

    public static EstadoDto fromEstado(Estado estado) {
        return new EstadoDto(
                estado.getId().toString(),
                estado.getNome(),
                estado.getSigla());
    }
}
