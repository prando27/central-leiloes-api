package br.com.centralleiloes.centralleiloesapi.repository;

import br.com.centralleiloes.centralleiloesapi.domain.Estado;
import org.springframework.data.repository.CrudRepository;

public interface EstadoRepository extends CrudRepository<Estado, Integer> {
}
